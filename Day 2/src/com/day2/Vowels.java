package com.day2;

	import java.util.Scanner;
	public class Vowels{

		public static void main(String[] args) {
			Scanner in = new Scanner(System.in);
			System.out.println("String");
			String str = in.nextLine();
			System.out.println("number of vowels in a string:" + vowelsCount(str));
		}

		public static int vowelsCount(String str) {
			int count = 0;
			for (int i = 0; i < str.length(); i++) {
				if (str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i' || str.charAt(i) == 'o'
						|| str.charAt(i) == 'u') {
					count++;
				}
			}
			return count;
		}

	}