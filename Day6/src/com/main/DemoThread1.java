package com.main;


public class DemoThread1 implements Runnable {

	public static void main(String[] args) {
		DemoThread1 obj1 = new DemoThread1();
		DemoThread1 obj2= new DemoThread1();
		DemoThread1 obj3= new DemoThread1();
		obj1.run();
		
		obj1 = null;
		obj2 = null;
		obj3 = null;
	}

	public DemoThread1() {

		UserCode1Main t1 = new UserCode1Main();

		System.out.println("Name of t1:" + t1.getName());

		t1.start();

	}

	@Override
	public void run() {
		System.out.println("running child Thread in loop:");
		int counter = 1;
		while (counter < 11) {
			System.out.println(counter);
		counter = counter + 1;
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		}
	}

}