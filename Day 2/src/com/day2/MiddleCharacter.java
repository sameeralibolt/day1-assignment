package com.day2;

	import java.util.Scanner;
	public class MiddleCharacter {

		public static void main(String[] args) {
	        Scanner k=new Scanner(System.in);
	        System.out.println("input:");
	        String str=k.nextLine();
	        System.out.println("The middle character in string:"+middle(str));
		}

	public static String middle(String str) {
		int length,pos;
		if(str.length()%2==0) {
			pos=str.length()/2-1;
			length=2;
		}
		else {
			pos=str.length()/2;
			length=1;
		}
		return str.substring(pos,pos+length);	
		}
	}
	  
