package asciichar;

package asciichar;

import java.util.Scanner;

public class Asciicharecter {

	public static void main(String[] args) {
		System.out.println("Enter a Character: ");
		Scanner sc = new Scanner(System.in);
		char ch = sc.next().charAt(0);
		int asciiValue = ch;
		System.out.println("ASCII Value of " + ch + "  is: " + asciiValue);
		sc.close();

	}

}
