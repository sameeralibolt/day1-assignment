package newproject;

public class AverageCalculator {
	public int avgCalculator(int num1,int num2,int num3) {
		return (num1+num2+num3)/3;
		}
	public void swapNumbers(int num4,int num5) {
		System.out.println("------Before swapping----");
		System.out.println("num4 ="+ num4);
		System.out.println("num5 ="+ num5);
		int temp;
		num4=num5;
		num5=temp;
		System.out.println("------After swapping----");
		System.out.println("num4 ="+ num4);
		System.out.println("num5 ="+ num5);
	}
	public void display() {
	for(int i=1; i<=100; i++) {
		if(i%3==0 && i%5==0) {
			System.out.println("Fizz Buzz");
		}else if(i%5==0) {
			System.out.println("Buzz");
		}else if(i%3==0) {
			System.out.println("Fizz");
		}else {
			System.out.println(i);
		}